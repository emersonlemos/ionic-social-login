# Ionic Social Login #

[EM CONSTRUÇÃO]
Este é um projeto para aprender a configurar o login via Facebook e via Google de maneira nativa.   
**NOTA: NÃO ESTÁ FUNCIONANDO NO IOS. USAR [Cordova Plugin Facebook 4](https://github.com/jeduan/cordova-plugin-facebook4)**

### Características do Projeto ###

* Versão do Ionic: 1.3
* Versão do Facebook API: 2.6
* Varsão do Google API: 

### Integração com o Facebook ###
#### Requisitos ####
* Após ter um aplicativo ionic já configurado, será necessário criar um aplicativo na [conta de desenvolvedor](http://www.developers.facebook.com) do Facebook. Feito isso, você terá acesso ao APP_ID do mesmo.   
* A integração se dará pelo [Apache Cordova Facebook Plugin](https://github.com/Wizcorp/phonegap-facebook-plugin). As configurações para iOS necessitam de um pouco mais de cuidado. Tentarei explicar aqui, mas no link há mais detalhes.   

#### Configurando a API do Facebook ####
Ao gerar o aplicativo, você deverá gerar uma nova plataforma para fazer a conexão com o aplicativo. Será necessário gerar uma plataforma para iOS e uma para Android.   
##### Android #####
Para gerar uma plataforma Android, será necessário saber o **package name** do aplicativo. Ele se encontrar no **AndroidManifest.xml**, dentro de platforms/Android. Além disso, será necessário indicar a **activity principal** do app. Ela também se encontra neste arquivo.   

Trecho exemplo do AndroidManifest.xml
```xml
<manifest (...)
    **package="br.com.renanbarbieri.sociallogin"**
    xmlns:android="http://schemas.android.com/apk/res/android">
    (...)
    <application (...)>
        <activity (...)
                    **android:name="MainActivity"**>
            <intent-filter android:label="@string/launcher_name">
                <action android:name="android.intent.action.MAIN" />
                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
    </application>
</manifest>
```   
Configuração do Facebook Developers
![Configuração do App Android no Facebook](https://bitbucket.org/repo/bLrGG9/images/3117470391-Captura%20de%20tela%20de%202016-06-20%2023-47-24.png)   
   
##### iOS #####
   
   
#### Configurando o plugin ####
O plugin, devido a um problema com o Xcode, será necessário clonar o projeto do repositório dele. Sendo assim, execute os seguintes passos      
  
* Clone o projeto em uma pasta (pode usar o terminal para isso): 
``` git clone https://github.com/Wizcorp/phonegap-facebook-plugin.git ```
* Adicione as plataformas
``` ionic platform add ios; ionic platform add android ```
* Adicione o plugin com o seguinte comando
``` cordova -d plugin add /pasta_para_plugin_clonado/phonegap-facebook-plugin --variable APP_ID="APP_ID_GERADO"--variable APP_NAME="DISPLAY_NAME_FACEBOOK" ```